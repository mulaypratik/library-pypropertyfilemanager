'''
Created on Apr 16, 2018

@author: Pratik
'''
from PropertyFileManager.ConstantsClassPFM import ConstantsClassPFM


class PropertyFileTester:
    'PropertyFileTester class to test various operations of PropertyFileOperator from outside'
    
    ConstantsClassPFM.pfo.__init__("config.ini")
    print ConstantsClassPFM.pfo.readProperty("USER_DETAILS", "USERNAME")
    print "Prop value before change: ", ConstantsClassPFM.pfo.readProperty("USER_DETAILS", "PASSWORD")
    print ConstantsClassPFM.pfo.writeProperty("USER_DETAILS", "PASSWORD", "pqr@456")
    print "Prop value after change: ", ConstantsClassPFM.pfo.readProperty("USER_DETAILS", "PASSWORD")
