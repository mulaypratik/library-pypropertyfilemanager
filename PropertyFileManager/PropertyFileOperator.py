'''
Created on Apr 16, 2018

@author: Pratik
'''
import configparser


class PropertyFileOperator:
    'This class contains all the configuration file related operations.'

    def __init__(self, ConfigFilePath):
        try:
            self.__configFilePath = ConfigFilePath
            
            self.__config = configparser.ConfigParser()
            
            if self.__configFilePath != None:
                self.__config.read(self.__configFilePath)
            
            return None
        except Exception as e:
            print e
            
            return e
        
    def readProperty(self, SectionName, PropertyName):
        try:
            return self.__config[SectionName][PropertyName]
        except Exception as e:
            print e
            
            return e
    
    def writeProperty(self, SectionName, PropertyName, Value):
        try:
            self.__config[SectionName][PropertyName] = Value
            
            with open(self.__configFilePath, 'w') as configfile:
                self.__config.write(configfile)
        
            return None
        except Exception as e:
            print e
            
            return e
