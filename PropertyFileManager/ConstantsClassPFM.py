'''
Created on Apr 16, 2018

@author: Pratik
'''
from PropertyFileManager.PropertyFileOperator import PropertyFileOperator


class ConstantsClassPFM:
    'This class contains an object of PropertyFileOperator class which can be used as a static throughout the project.'
    
    pfo = PropertyFileOperator(None)
